import 'package:bmi_calculator/pages/result_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../components/cards/ReusableCard.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bmi_calculator/calculator_brain.dart';
import '../../components/cards/IconContent.dart';

const bottomContainerHeight = 80.0;
const inactiveCardColor = Color(0xFF111328);
const activeCardColor = Color(0xFF1D1E33);
const bottomContainerColor = Color(0xFFEB1555);
enum Gender {
  male,
  female
}

class InputPage extends StatefulWidget {
  @override
  State<InputPage> createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  Gender gender;
  int height = 180;
  int weight = 60;
  int age = 20;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
      ),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: Column(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: ReusableCard(
                          onPress: () => setState(() {
                          gender = Gender.male;
                          }),
                          colour:  gender == Gender.male ? activeCardColor : inactiveCardColor,
                          cardChild: IconContent(icon: FontAwesomeIcons.mars, label: 'MALE')
                        ),
                      ),
                      Expanded(
                        child: ReusableCard(
                          onPress: () => setState(() {
                            gender = Gender.female;
                          }),
                          colour:  gender == Gender.female ? activeCardColor : inactiveCardColor,
                          cardChild: IconContent(icon: FontAwesomeIcons.venus, label: 'FEMALE')
                        ),
                       )
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Expanded(
                  child: Row(
                    children: [
                      Expanded(child: ReusableCard(
                        colour: activeCardColor,
                        cardChild: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('HEIGHT', style: TextStyle(
                              fontSize: 18.0,
                              color: Color(0xFF8D8E98)
                            )),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.baseline,
                              textBaseline: TextBaseline.alphabetic,
                              children: [
                                Text(height.toString(), style: TextStyle(
                                  fontSize: 50.0,
                                  fontWeight: FontWeight.w900
                                )),
                                Text('cm', style: TextStyle(
                                  fontSize: 18.0,
                                  color: Color(0xFF8D8E98)
                                ))
                              ],
                            ),
                            SliderTheme(
                              data: SliderTheme.of(context).copyWith(
                              thumbColor: Color(0xFFEB1555),
                              activeTrackColor: Color(0xFFEB1555),
                              inactiveTrackColor: Color(0xFF8D8E98),
                              overlayColor: Color(0x29EB1555),
                              thumbShape: RoundSliderThumbShape(enabledThumbRadius: 15.0),
                              overlayShape: RoundSliderOverlayShape(overlayRadius: 24.0),
                              ),
                              child: Slider(
                                value: height.toDouble(),
                                min: 120,
                                max: 220,
                                onChanged: (double newValue) {
                                  setState(() {
                                    height = newValue.round();
                                  });
                                },
                              ),
                            )
                          ]
                        ),
                      ) )
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Expanded(
                      child: ReusableCard(
                        cardChild: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('WEIGHT', style: TextStyle(
                                fontSize: 18.0,
                                color: Color(0xFF8D8E98)
                              )),
                              Text(weight.toString(), style: TextStyle(
                                fontSize: 50.0,
                                fontWeight: FontWeight.w900
                              )),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  RoundIconButton(
                                    icon: FontAwesomeIcons.minus,
                                    onPress: () => setState(() {
                                      weight--;
                                    }),
                                  ),
                                  SizedBox(width: 10.0),
                                  RoundIconButton(
                                    icon: FontAwesomeIcons.plus,
                                    onPress: () => setState(() {
                                      weight++;
                                    }),
                                  )
                                ],
                              )
                            ],
                          )),
                        colour: activeCardColor
                      ) ),
                  Expanded(
                      child: ReusableCard(
                          cardChild: Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text('AGE', style: TextStyle(
                                      fontSize: 18.0,
                                      color: Color(0xFF8D8E98)
                                  )),
                                  Text(age.toString(), style: TextStyle(
                                      fontSize: 50.0,
                                      fontWeight: FontWeight.w900
                                  )),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      RoundIconButton(
                                        icon: FontAwesomeIcons.minus,
                                        onPress: () => setState(() {
                                          age--;
                                        }),
                                      ),
                                      SizedBox(width: 10.0),
                                      RoundIconButton(
                                        icon: FontAwesomeIcons.plus,
                                        onPress: () => setState(() {
                                          age++;
                                        }),
                                      )
                                    ],
                                  )
                                ],
                              )),
                          colour: activeCardColor
                      ) ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                CalculatorBrain calc = CalculatorBrain(height: height, weight: weight);
                Navigator.push(context, MaterialPageRoute(builder: (context) => ResultPage(
                    bmiResult: calc.calculateBMI(),
                    resultText: calc.getResult(),
                    interpretation: calc.getInterpretation()
                )));
              },
              child: Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 10.0),
                height: bottomContainerHeight,
                child: Center(
                  child: Text('CALCULATE', style: TextStyle(
                    fontSize: 25.0,
                    fontWeight: FontWeight.bold
                  )),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4.0),
                  color: bottomContainerColor
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class RoundIconButton extends StatelessWidget {
  const RoundIconButton({this.icon, this.onPress});

  final IconData icon;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onPress,
      child: Icon(icon),
      elevation: 6.0,
      shape: CircleBorder(),
      constraints: BoxConstraints.tightFor(
        width: 56.0,
        height: 56.0,
      ),
      fillColor: Color(0xFF4C4F5E),
    );
  }
}


