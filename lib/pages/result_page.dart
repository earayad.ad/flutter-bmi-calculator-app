import 'package:bmi_calculator/components/cards/ReusableCard.dart';
import 'package:flutter/material.dart';


class ResultPage extends StatelessWidget {

  ResultPage({this.bmiResult, this.resultText, this.interpretation});

  final String bmiResult;
  final String resultText;
  final String interpretation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: Container(
                alignment: Alignment.bottomLeft,
                margin: EdgeInsets.only(left: 10.0),
                child: Text('Your Result', style: TextStyle(fontSize: 50.0, fontWeight: FontWeight.bold),),
              ),
            ),
            Expanded(
              flex: 5,
              child: Container(
                margin: EdgeInsets.all(10.0),
                child: ReusableCard(
                  colour: Color(0xFF1D1E33),
                  cardChild: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(resultText, style: TextStyle(
                          fontSize: 22.0,
                          color: Color(0xFF24D876),
                          fontWeight: FontWeight.bold
                        )),
                        Text(bmiResult, style: TextStyle(
                          fontSize: 100.0,
                          fontWeight: FontWeight.bold
                        )),
                        Text(interpretation, style: TextStyle(
                          fontSize: 22.0
                        ), textAlign: TextAlign.center,)
                      ],
                    ),
                  ),
                ),
              )
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Expanded(
                  child: Container(
                    child: Text('Re-Calculate', style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold
                    )),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Color(0xFFEB1555),
                      borderRadius: BorderRadius.circular(4.0)
                    )
                  ),
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
}

